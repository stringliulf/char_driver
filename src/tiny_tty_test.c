#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <termios.h>
#include <sys/types.h>
#include <sys/stat.h>

#define max_buffer_size 100	/*recv buffer size */

int open_serial(int k)
{
	char pathname[20] = { 0 };
	int ret;

//    sprintf(pathname, "/dev/ttytiny0");
	ret = open("/dev/ttyUSB0", O_RDWR | O_NOCTTY);
	if (ret == -1) {
		perror("open error");
		exit(-1);
	} else
		printf("Open %s success\n", pathname);

	return ret;
}

int main()
{
	int fd;
	ssize_t n;
	char recv[max_buffer_size] = { 0 };
	struct termios opt;
	char write_buf[] = "abcdefghijklmnopqrstuvwxyz1234567890";

	fd = open_serial(0);	/*open device 0 */

	tcgetattr(fd, &opt);
	cfmakeraw(&opt);
	tcsetattr(fd, TCSANOW, &opt);

	n = write(fd, write_buf, sizeof write_buf);
	printf("write ret:%ld\n", n);

	printf("ready for receiving data...\n");
	n = read(fd, recv, sizeof(recv));

	if (n == -1) {
		perror("read error");
		exit(-1);
	}

	printf("data receive:%ld\n", n);
	printf("The data received is %s\n", recv);
	if (close(fd) == -1)
		perror("close error");

	return 0;
}
