/* chardev.c
 *
 * Copyright 2018 liulf
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

int main(int argc, char *argv[])
{
	int fd = -1;
	ssize_t size = 0;
	int tty_cnt = 0;
	char devname[100];
	char read_buf[100];

	for (tty_cnt = 0; tty_cnt < 1; tty_cnt++) {
		snprintf(devname, sizeof devname, "/dev/ttytiny%d", tty_cnt);
		fd = open(devname, O_RDWR);
		if (-1 == fd) {
			printf("open file error\n");
			return -1;
		}

		printf("open\n");

		size = write(fd, devname, sizeof devname);
		printf("write ret:%ld\n", size);

		size = read(fd, read_buf, sizeof read_buf);
		printf("read ret:%ld :%s\n", size, read_buf);

		if (fd != -1) {
			printf("close file\n");
			close(fd);
			fd = -1;
		}
	}

	return EXIT_SUCCESS;
}
