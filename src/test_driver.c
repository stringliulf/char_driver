#include<linux/init.h>
#include<linux/device.h>
#include <linux/slab.h>
#include<linux/module.h>

extern struct bus_type testbus;
struct device_driver *mydrv = NULL;

int mydrv_probe(struct device *dev)
{
	printk(KERN_INFO "%s\n", __func__);
	return 0;
}

int mydrv_remove(struct device *dev)
{
	printk(KERN_INFO "%s\n", __func__);
	return 0;
}

static int mydrv_init(void)
{
	int ret = 0;

	mydrv = kzalloc(sizeof(*mydrv), GFP_KERNEL);
	if (!mydrv) {
		ret = -ENOMEM;
		printk(KERN_ALERT "%s kzalloc error\n", __func__);
	} else {
		mydrv->name = "testdevice";
		mydrv->bus = &testbus;
		mydrv->probe = mydrv_probe;
		mydrv->remove = mydrv_remove;
		ret = driver_register(mydrv);

		if (!ret) {
			printk(KERN_INFO "registered new driver:%s success\n",
			       mydrv->name);
		} else {
			printk(KERN_ALERT "%s registered new driver:%s error\n",
			       __func__, mydrv->name);
			kfree(mydrv);
		}
	}

	return ret;
}

static void mydrv_exit(void)
{
	printk(KERN_ALERT "%s\n", __func__);
	driver_unregister(mydrv);
	kfree(mydrv);
}

module_init(mydrv_init);
module_exit(mydrv_exit);
MODULE_LICENSE("GPL");
