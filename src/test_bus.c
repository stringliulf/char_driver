#include<linux/init.h>
#include<linux/module.h>
#include<linux/device.h>

int testbus_match(struct device *dev, struct device_driver *drv);

struct bus_type testbus = {
	.name = "testbus",
	.match = testbus_match,
};

EXPORT_SYMBOL(testbus);

int testbus_match(struct device *dev, struct device_driver *drv)
{
	if (strncmp(drv->name, dev->kobj.name, strlen(drv->name)) == 0) {
		printk(KERN_INFO "match success %s\n", drv->name);
		return 1;
	} else {
#if 0
		printk(KERN_INFO "not match %s\n", drv->name);
#endif
		return 0;
	}
}

static int testbus_init(void)
{
	int ret;

	ret = bus_register(&testbus);
	if (ret != 0) {
		printk(KERN_ALERT "registe %s error %s\n", testbus.name,
		       __func__);
	} else {
		printk(KERN_INFO "registe %s success %s\n", testbus.name,
		       __func__);
	}

	return ret;
}

static void testbus_exit(void)
{
	bus_unregister(&testbus);
	printk(KERN_ALERT "unregiste %s %s\n", __func__, testbus.name);
}

module_init(testbus_init);
module_exit(testbus_exit);
MODULE_LICENSE("GPL");
