#include<linux/init.h>
#include<linux/module.h>
#include<linux/device.h>

#include <linux/slab.h>

extern struct bus_type testbus;
struct device *mydev;

static void mydev_release(struct device *dev)
{
	printk(KERN_INFO "%s\n", __func__);
}

static int mydev_init(void)
{
	int ret = 0;

	mydev = kzalloc(sizeof(*mydev), GFP_KERNEL);

	if (!mydev) {
		printk(KERN_ALERT "%s kzalloc error\n", __func__);
		ret = -ENOMEM;
	} else {
		mydev->init_name = "testdevice";
		mydev->bus = &testbus;
		mydev->release = mydev_release;

		ret = device_register(mydev);

		if (!ret) {
			printk(KERN_INFO "registered new device:%s success\n",
			       mydev->kobj.name);
		} else {
			kfree(mydev);
			printk(KERN_ALERT "%s registered new device:%s error\n",
			       __func__, mydev->init_name);
		}
	}

	return ret;
}

static void mydev_exit(void)
{
	printk(KERN_INFO "%s\n", __func__);
	device_unregister(mydev);
	kfree(mydev);
}

module_init(mydev_init);
module_exit(mydev_exit);
MODULE_LICENSE("GPL");
