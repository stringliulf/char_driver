#include <linux/module.h>
#include <linux/device.h>
#include <linux/cdev.h>
#include <linux/kernel.h>

MODULE_LICENSE("Dual BSD/GPL");
struct timer_list timer;

struct test_type {
	int index;
	struct timer_list time;
} test;

void callback(struct timer_list *timer_list)
{
	struct test_type *t;
	mod_timer(&test.time, jiffies + HZ);
	t = container_of(timer_list, struct test_type, time);

	printk(KERN_INFO "%s t->index:%d \n", __func__, t->index++);
}

static int hello_init(void)
{
	test.index = 0;
	timer_setup(&test.time, callback, 1);
	mod_timer(&test.time, jiffies + HZ);
	printk(KERN_INFO "%s\n", __func__);
	return 0;
}

static void hello_exit(void)
{
	del_timer(&test.time);
	printk(KERN_INFO "%s\n", __func__);
}

module_init(hello_init);
module_exit(hello_exit);
