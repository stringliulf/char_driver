WARNINGS = -Wall
DEBUG = -ggdb -fno-omit-frame-pointer
OPTIMIZE = -O2
OUTDIR = out

all: $(OUTDIR) .test.stamp .tty_test.stamp driver

$(OUTDIR):
	mkdir $@

.test.stamp: src/char_test.c
	$(CC) -o $(OUTDIR)/$(basename $(notdir $<)) $(WARNINGS) $(DEBUG) $(OPTIMIZE) $< 
	touch $@

.tty_test.stamp: src/tiny_tty_test.c
	$(CC) -o $(OUTDIR)/$(basename $(notdir $<)) $(WARNINGS) $(DEBUG) $(OPTIMIZE) $< 
	touch $@

# Builder will call this to install the application before running.
install:
	echo "Installing is not supported"

# Builder uses this target to run your application.
run:
	echo "Run is not supported"

#build driver 
PWD := $(shell pwd)
obj-m :=src/char_driver.o
obj-m +=src/test_bus.o
obj-m +=src/test_driver.o
obj-m +=src/test_device.o
obj-m +=src/tiny_tty.o
obj-m +=src/timer_driver.o
KERNELDIR := /lib/modules/$(shell uname -r)/build
driver:
	$(MAKE) -C $(KERNELDIR) M=$(PWD) modules
	@find ./src  -name "*.ko" |  xargs  -I  '{}'  ln  -f {} $(OUTDIR) 

clean:
	rm -f ./*/.*.cmd
	rm -f ./*/*.o
	rm -f ./*/*.ko
	rm -f ./*/*.mod.c
	rm -rf .cache.mk .tmp_versions Module.symvers modules.order
	rm -f .*.stamp 
	rm -f $(OUTDIR)/*
