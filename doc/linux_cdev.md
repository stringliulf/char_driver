# 字符驱动
字符驱动是linux驱动学习的第一站，该代码完成了一个最简单的字符驱动以及一个最小化打测试代码
linux提供了很好打驱动框架字符驱动只需要填充结构体中相应打成员，然后将其注册到系统中
linux在字符驱动中没有自动创建设备节点，需要单独的进行处理
开始写这个代码的时候有两个目的想做一下测试，一个是linux设备号的自动分配，一个是linux自动创建设备文件

#### 测试代码 
> 代码仅做了open的测试
```c
int main (int argc, char *argv[])
{
	int fd = -1;

	fd = open("/dev/chartest1",O_RDWR);
	if(-1 == fd ){
		printf("open file error\n");
		return -1;
	}
	printf("open file success\n");
	if(fd != -1){
		printf("close file\n");
		close(fd);
		fd = -1;
	}
	return EXIT_SUCCESS;
}
```

#### 驱动代码 
> 下面打代码只做示例，具体代码请参考代码文件char_driver.c 
```c
static dev_t major = -1;
static int const devcnt = 1;
struct cdev cdev;
struct device *char_dev = NULL;
static struct class *chartest_class;

/*定义一个 file_operations 结构体*/
static const struct file_operations chardev_dev_fops = {
	.owner		= THIS_MODULE,
	.llseek		= no_llseek,
	.read		= chardev_dev_read,
	.write		= chardev_dev_write,
	.unlocked_ioctl	= chardev_dev_ioctl,
	.open		= chardev_dev_open,
	.release	= chardev_dev_release,
};

static int hello_init(void)
{
	unsigned int err;

	cdev_init(&cdev, &chardev_dev_fops); /*初始化字符驱动*/
	err = alloc_chrdev_region(&major, 0, devcnt , "unused"); /*动态申请设备号*/
	err = cdev_add(&cdev, major, 1);/*将设备注册到系统中*/
	chartest_class = class_create(THIS_MODULE, "testclass"); /*创建设备文件:先创建一个class然后在class下创建一个设备文件*/
	char_dev = device_create(chartest_class, NULL, major, NULL, "%s%d", "chartest", 1);

	return 0;
}

static void hello_exit(void)
{
	device_destroy(chartest_class, major);
	class_destroy(chartest_class);
	unregister_chrdev(major, "test");
	major = -1;
	cdev_del(&cdev);

	printk(KERN_INFO "%s\n",__func__);
}

module_init(hello_init);
module_exit(hello_exit);
```
